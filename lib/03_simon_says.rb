def echo(word)
  word
end

def shout(words)
  words.upcase
end

def repeat(word, a = 2)
  i = 0
  arr = []
  str = ""
  while i < a
    arr << word
    i += 1
  end
  arr.each do |el|
    str << el + " "
  end
  str.chop
end

def start_of_word(word, a)
  x = a - 1
  word[0..x]
end

def first_word(str)
  arr = str.split
  arr[0].to_s
end

def titleize(str)
  arr = str.split
  little_words = ["and", "over", "the"]
  arr.each_with_index do |el, i|
    if i == 0
      el.capitalize!
    elsif el == "and" || el == "over" || el == "the"
      el.downcase
    else
      el.capitalize!
    end
  end
  arr.join(" ")
end
